import os
from django.conf import settings
from telegram.ext import CallbackContext
import datetime
from datetime import datetime, timedelta
from django.conf import settings
from django.db.models import Count
from asgiref.sync import sync_to_async

from telegram.constants import ParseMode

# from pprint import pprint
from core import monitoring
from core.bot_telegram import utils
from core.models import (
    ChatUser,
)  # Replace 'myapp' with the actual name of your Django app

previous_servers_status = {}
HTML_DAILY_STATS_FILE = "daily_stats.html"


@sync_to_async
def save_stats(context: CallbackContext):
    # Calculate the timestamp for 24 hours ago
    oneweek_ago = datetime.now() - timedelta(days=7)

    # Filter ChatUser objects based on last interaction and creation timestamps
    interacted_users_count = ChatUser.objects.filter(
        last_interacted_at__gte=oneweek_ago
    ).count()
    created_users_count = ChatUser.objects.filter(created_at__gte=oneweek_ago).count()

    lang_users_count = (
        ChatUser.objects.values("lang").annotate(count=Count("lang")).order_by()
    )
    lang_users_count = {entry["lang"]: entry["count"] for entry in lang_users_count}
    nolang_users_count = ChatUser.objects.filter(lang__isnull=True).count()
    lang_users_count[None] = nolang_users_count
    total_users_count = ChatUser.objects.count()
    message = "📊<b>@G1Superbot Statistics</b>📊"
    message += f"\n⌨️Unique Interacting Users (Last Week): {interacted_users_count}"
    message += f"\n👼New Wallet Accounts (Last Week): {created_users_count}"
    message += f"\n👯Total Accounts: {total_users_count}"
    message += f"\n🎓Accounts per language: {lang_users_count}"

    message += "\n\n📊<b>Estadísticas @G1Superbot</b>📊"
    message += f"\n⌨️Usuarios interactuando (Última Semana): {interacted_users_count}"
    message += f"\n👼Nuevos monederos (Última Semana): {created_users_count}"
    message += f"\n👯Total Cuentas: {total_users_count}"
    message += f"\n🎓Cuentas por idioma: {lang_users_count}"

    with open(HTML_DAILY_STATS_FILE, "w", encoding="utf-8") as file:
        file.write(message)


async def send_stats(context: CallbackContext):
    print(f"Reading HTML daily stats: {HTML_DAILY_STATS_FILE}")
    if os.path.exists(HTML_DAILY_STATS_FILE):
        with open(HTML_DAILY_STATS_FILE, "r", encoding="utf-8") as file:
            html_content = file.read()
            # print('html', html_content)
            await context.bot.send_message(
                chat_id=settings.TELEGRAM_DEVELOPER_CHAT_ID,
                text=html_content,
                parse_mode=ParseMode.HTML,
            )
        os.remove(HTML_DAILY_STATS_FILE)  # Delete the file after reading
    else:
        print(f"Daily Stats {HTML_DAILY_STATS_FILE} not found.")


async def check_gva_servers(context: CallbackContext):
    servers_status = monitoring.check_gva_servers()
    # sorted_servers = sorted(servers_status, key=servers_status.get)
    MAX_TIME_LAG = 5  # Max time response in seconds

    for server, status in servers_status.items():
        print(f"{server} - {status}")
        msg = ""
        global previous_servers_status
        # Remove "https://" from the beginning
        server = server.split("://")[1].split("/")[0]
        if settings.GVA_SERVERS[
            server
        ]:  # if it's the main Duniter server, we show the reports
            if server not in previous_servers_status:
                previous_servers_status[server] = {"ping": 1}  # initial status
            if status["ping"] == -1 and previous_servers_status[server] != -1:
                msg = "it's not returning a 200 OK"
            elif (
                "unsynced" in status
                and "unsynced" not in previous_servers_status[server]
            ):
                msg = "has become unsynced with the blockchain."
            elif (
                status["ping"] > MAX_TIME_LAG
                and previous_servers_status[server]["ping"] <= MAX_TIME_LAG
            ):
                msg = f"is answering with a lag of {status} > {MAX_TIME_LAG}s"
            if msg:
                await context.bot.send_message(
                    chat_id=settings.TELEGRAM_DEVELOPER_CHAT_ID,
                    text=f"<b>{server}</b> {msg}",
                    parse_mode=ParseMode.HTML,
                )
    previous_servers_status = servers_status
