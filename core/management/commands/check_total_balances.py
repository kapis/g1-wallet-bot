import os

from django.conf import settings

from django.core.management.base import CommandError, BaseCommand

from core.bot_telegram import utils

class Command(BaseCommand):
    help = 'Starts the Telegram bot'

    def handle(self, *args, **options):
        import json

        # Open the JSON file
        path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'balances.json'))

        with open(path, 'r') as f:
            # Load the JSON data from the file
            data = json.load(f)

        # Access the 'balances' list in the loaded JSON data
        balances = data['data']['balances']

        # Sum the amounts in the 'amount' field of the balances list
        total_balance = sum(b['amount'] for b in balances if b and 'amount' in b)

        print(f"Total balance: {total_balance/100}")
            