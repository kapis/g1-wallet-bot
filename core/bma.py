#Code taken from Silkaj 0.11

def amount_in_current_base(source):
    return source.amount * 10 ** source.base
    

# from click import pass_context
from duniterpy.api import bma
from duniterpy.documents.transaction import InputSource, OutputSource
from duniterpy.api.client import Client
from duniterpy.api import endpoint as ep


async def get_balance(node, pubkey):
    endpoint = ep.endpoint(node)
    client = Client(endpoint)
    sources = client(bma.tx.sources, pubkey)

    listinput = []
    amount = 0
    for source in sources["sources"]:
        if source["conditions"] == f"SIG({pubkey})":
            listinput.append(
                InputSource(
                    amount=source["amount"],
                    base=source["base"],
                    source=source["type"],
                    origin_id=source["identifier"],
                    index=source["noffset"],
                )
            )
            amount += amount_in_current_base(listinput[-1])

    return amount/100